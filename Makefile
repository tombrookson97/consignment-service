configfile=config.env

build:
	protoc -I. --go_out=plugins=micro:. \
	  proto/consignment/consignment.proto
	GOOS=linux GOARCH=amd64 go build -o bin/service-consignment -a -installsuffix cgo main.go repository.go datastore.go handler.go && \
	docker build -t service-consignment .

#run:
#    docker run -p 50051:50051 -e MICRO_SERVER_ADDRESS=:50051 service-consignment
module bitbucket.org/tombrookson/consignment-service

go 1.13

require (
	bitbucket.org/tombrookson/consignment-vessel-service v0.0.0-20191130220434-44136f3ca17b
	github.com/golang/protobuf v1.3.2
	github.com/golang/snappy v0.0.1 // indirect
	github.com/micro/go-micro v1.17.1
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.1.3
	golang.org/x/crypto v0.0.0-20191202143827-86a70503ff7e // indirect
	golang.org/x/net v0.0.0-20191126235420-ef20fe5d7933
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/tools v0.0.0-20191203134012-c197fd4bf371 // indirect
	google.golang.org/genproto v0.0.0-20191203145615-049a07e0debe // indirect
)
